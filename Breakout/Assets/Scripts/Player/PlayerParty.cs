﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParty : MonoBehaviour
{
    public static PlayerParty instance;
    public List<GameObject> members = new List<GameObject>();
    public float partySpacing = 0.6f;
    private bool _alternateSides = true;
    private BoxCollider _collider;

    //set the newPos to alternating sides when adding new members
    private Vector3 newPos
    {
        get
        {
            return new Vector3(members.Count / 2 * (_alternateSides ? partySpacing : -partySpacing), 0, 0);
        }
    }
    private void Awake()
    {
        //creates a singleton of this object
        if (instance == null)
            instance = this;
        else
            Destroy(this.gameObject);

        //line up the intial party
        float xPos = 0;
        for (int i = 1; i < members.Count; i++)
        {
            xPos = _alternateSides ? i * partySpacing : -xPos;
            members[i].transform.localPosition = new Vector3(xPos, 0, 0);
            _alternateSides = !_alternateSides;
        }
    }

    public void AddMember(GameObject member)
    {
        members.Add(member);
        member.transform.SetParent(this.transform);

        //position the character spaced correctly apart
        member.gameObject.transform.localPosition = newPos;
        _alternateSides = !_alternateSides;
    }

    public void RemoveMember(GameObject member)
    {
        for (int i = 0; i < members.Count; i++)
        {
            if (members[i].activeInHierarchy)
                return;
        }
        GameManager.Instance.GameState("YOUR PARTY DIED!");
    }
}
