﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public float speed;

    private Rigidbody rb;
    private bool initialMove;
    private float _hAxis;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {


        //gets input for touch screens
        //if the left or right screen gets touched, move respectively to that side
        if (Input.touchCount > 0)
        {
            Vector3 touch = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, Camera.main.transform.position.z);
            Vector3 touchPos = Camera.main.ScreenToWorldPoint(touch);
            _hAxis = touchPos.x;
            return;
        }

        //get inputs
        _hAxis = Input.GetAxis("Horizontal");
    }

    void FixedUpdate()
    {
        MovePlayer();
    }

    void MovePlayer()
    {
        //set the target position to move to
        Vector3 targetPos = transform.right * speed * _hAxis * (Time.deltaTime * 10);
        //moves the player
        rb.velocity = targetPos;

        //spawn the intial projectile after the player has moved for the first time
        if (!initialMove && rb.velocity.x > 0)
        {
            GameManager.Instance.SpawnMainProjectile();
            initialMove = true;
        }
    }
}
