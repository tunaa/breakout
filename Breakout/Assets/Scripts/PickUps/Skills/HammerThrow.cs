﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerThrow : PickUpBase
{
    // Start is called before the first frame update
    public GameObject hammer;



    public override void DoAction()
    {
        foreach (var m in PlayerParty.instance.members)
        {
            if (m.activeInHierarchy)
            {
                Instantiate(hammer, m.transform.position + Vector3.forward, hammer.transform.rotation);
                m.GetComponentInChildren<Animator>().SetTrigger("Throw");
            }
        }
        base.DoAction();
    }
}
