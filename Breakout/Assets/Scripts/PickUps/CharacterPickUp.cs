﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterPickUp : PickUpBase
{
    public GameObject character;

    public override void DoAction()
    {
        GameObject c = Instantiate(character);
        PlayerParty.instance.AddMember(c);
        base.DoAction();
    }
}
