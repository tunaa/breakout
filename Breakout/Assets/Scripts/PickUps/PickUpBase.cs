﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpBase : MonoBehaviour
{
    virtual public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            DoAction();
    }

    virtual public void DoAction()
    {
        gameObject.SetActive(false);
    }
}
