﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickUp : PickUpBase
{
    public float healAmount = 3;
    public override void DoAction()
    {
        foreach (var m in PlayerParty.instance.members)
        {
            m.GetComponent<HealthBase>().Heal(healAmount);
        }
        base.DoAction();
    }
}
