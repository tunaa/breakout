﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableHealth : HealthBase
{
    public int scoreValue = 50;
    public List<GameObject> droppedItem = new List<GameObject>();
    public float chanceToSpawnItems = 0.15f;

    public override void Dead()
    {
        //spwan a item out of the list;
        float chance = Random.Range(0f, 1f);
        if (chance <= chanceToSpawnItems && droppedItem.Count > 0)
            Instantiate(getItem(), transform.position, transform.rotation);
        //Add scoreValue to the main score
        GameManager.Instance.AddScore(scoreValue);
        base.Dead();
    }

    GameObject getItem()
    {
        //get a random item in the list of available items
        return droppedItem[Random.Range(0, droppedItem.Count)];
    }
}
