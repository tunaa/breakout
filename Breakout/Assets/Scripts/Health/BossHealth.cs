﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHealth : BreakableHealth
{
    public Animator anim;

    public override void Damage(float damageAmount)
    {
        //play the hit animation
        anim.SetTrigger("Hit");
        base.Damage(damageAmount);
    }
    public override void Dead()
    {
        GameManager.Instance.RemoveBoss(this);
        base.Dead();
    }
}
