﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBase : MonoBehaviour
{
    public float maxHealth = 1;
    //public HealthBar healthBar;
    public float currentHealth;
    public GameObject dmgEffect;
    public GameObject deathEffect;

    [Header("HealthBar")]
    public Gradient gradient;

    [Header("Death")]
    public AudioClip deathSound;
    public Renderer _renderer;

    void Start()
    {
        initilize();
    }

    public virtual void initilize()
    {
        if (currentHealth == 0)
            currentHealth = maxHealth;
        SetMatColour();
    }

    public void Heal(float healAmount)
    {
        //check to see if its below max then heal
        if (currentHealth < maxHealth)
        {
            currentHealth += healAmount;
            SetMatColour();
        }
        else if (currentHealth >= maxHealth)
            currentHealth = maxHealth;
    }

    public virtual void Damage(float damageAmount)
    {
        //take damage if health is higher than 0
        if (currentHealth > 0)
        {
            currentHealth -= damageAmount;
            SetMatColour();
        }

        if (currentHealth <= 0)
        {
            Dead();
        }
    }
    void SetMatColour()
    {
        //set the material to a color on the gradient based on hp %
        if (_renderer)
            _renderer.material.color = gradient.Evaluate(currentHealth / maxHealth);
    }

    public virtual void Dead()
    {
        // deathEffect.SetActive(true);
        // //make sure the parent is null so that the effect can be seen
        // deathEffect.transform.parent = null;
        // AudioSource.PlayClipAtPoint(deathSound, transform.position);
        gameObject.SetActive(false);
    }
}
