﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHealth : HealthBase
{
    public override void Dead()
    {
        PlayerParty.instance.RemoveMember(this.gameObject);
        base.Dead();
    }
}
