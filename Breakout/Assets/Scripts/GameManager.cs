﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public int lives = 3;
    public GameObject gameOverScreen;
    public int score;
    public List<BossHealth> bosses = new List<BossHealth>();// a list just incase we can do more than one boss per level.

    [Header("UI Text")]
    public TextMeshProUGUI livesCounter;
    public TextMeshProUGUI gameStateText;
    public TextMeshProUGUI gameScoreText;
    public TextMeshProUGUI finishScoreText;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this.gameObject);

        //display the lives onto the UI
        livesCounter.text = lives.ToString();
        //reset timescale
        Time.timeScale = 1;
    }

    public void SpawnMainProjectile()
    {
        //each boss shoots a projectile
        foreach (var boss in bosses)
        {
            BossBehaviour b = boss.GetComponent<BossBehaviour>();
            b.ShootProjectile();
        }
    }

    public void AddScore(int amount)
    {
        score += amount;
        try
        {
            gameScoreText.text = score.ToString();
        }
        catch
        {
            //allow the game to continue even there is no UI
            Debug.Log("Missing UI for score!");
        }
    }

    public void RemoveBoss(BossHealth boss)
    {
        bosses.Remove(boss);
        //if no bosses are left you win
        if (bosses.Count <= 0)
            GameState("YOU WIN!");
    }

    public void ReduceLives(int amount)
    {
        lives--;
        livesCounter.text = lives.ToString();

        if (lives <= 0)
            GameState("GAME OVER!");
    }

    public void AddLives(int amount)
    {
        lives++;
        livesCounter.text = lives.ToString();
    }

    public void GameState(string state)
    {
        //pauses the game
        Time.timeScale = 0;
        //update the state and score onto the game over screen
        gameStateText.text = state;
        finishScoreText.text = score.ToString();
        //turn on game over screen
        gameOverScreen.SetActive(true);
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
}
