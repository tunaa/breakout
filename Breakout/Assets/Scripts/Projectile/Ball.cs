﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : ProjectileBase
{
    [Header("Ball")]
    public bool startRandomDirection;
    public float speedIncrement = 0.05f;
    public Vector2 arcRange;//to determine the direction the ball will intially go

    private float timer;
    protected override void Start()
    {
        if (startRandomDirection) ShootRandomDirection();
        base.Start();
    }

    protected override void Update()
    {
        //increase the speed of the ball every second
        timer += Time.deltaTime;
        if (timer >= 1)
        {
            maxSpeed += speedIncrement;
            timer = 0;
        }
        base.Update();
    }

    void ShootRandomDirection()
    {
        //shoot the ball in a random direction downwards
        transform.rotation = Quaternion.Euler(transform.rotation.x, Random.Range(arcRange.x, arcRange.y), transform.rotation.z);
    }
}
