﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
[RequireComponent(typeof(Rigidbody))]
public class ProjectileBase : MonoBehaviour
{
    public float maxSpeed;
    public float bounceAmount = -1; //-1 for infinite
    public LayerMask targets;
    public float delay;

    protected Rigidbody _rb;
    private Vector3 _vel;
    private bool _intialMove;
    void Awake()
    {
        _rb = GetComponent<Rigidbody>();
    }
    protected virtual void Start()
    {
        //set the speed after a delay
        Invoke("Move", delay);
    }

    protected virtual void Update()
    {
        _vel = _rb.velocity;
        //move the projectile if it's stuck
        if (_vel == Vector3.zero && _intialMove)
            _rb.velocity = transform.forward * maxSpeed;
    }

    void Move()
    {
        //move the projectile in it's forward direction
        _rb.velocity = transform.forward * maxSpeed;
        _intialMove = true;
    }

    void OnCollisionEnter(Collision other)
    {
        //make sure it only bounces with targets
        if ((bounceAmount > 0 || bounceAmount == -1) && (targets.value & 1 << other.gameObject.layer) == 1 << other.gameObject.layer)
        {
            Bounce(other.GetContact(0).normal);
        }
        else
            Destroy(); //make it destroy itself instead
    }

    protected virtual void Bounce(Vector3 contactPoint)
    {
        //get the reflect position from the contacted normal
        Vector3 direction = Vector3.Reflect(_vel.normalized, contactPoint);
        //make sure it doesnt just bounce horizontally, give it a minimum angle
        direction = new Vector3(direction.x, 0, direction.z >= 0 ? Mathf.Clamp(direction.z, 0.15f, 1f) : Mathf.Clamp(direction.z, -1f, -0.15f));
        Debug.Log(direction);


        //change the direction
        _rb.velocity = direction * Mathf.Max(maxSpeed, _vel.magnitude);
        transform.rotation = Quaternion.LookRotation(direction);
        //reduce bounce;
        if (bounceAmount > 0)
            bounceAmount--;
    }

    protected virtual void Destroy()
    {
        this.gameObject.SetActive(false);
    }

}
