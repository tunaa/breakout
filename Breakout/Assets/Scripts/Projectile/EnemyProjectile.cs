﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : Ball
{
    public int DamageToLives = 1;


    protected override void Destroy()
    {
        //reduce the main lives
        GameManager.Instance.ReduceLives(DamageToLives);
        //spawn a new projectile
        GameManager.Instance.SpawnMainProjectile();
        base.Destroy();
    }
}
