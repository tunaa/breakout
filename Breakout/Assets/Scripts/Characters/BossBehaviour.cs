﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BossHealth))]
public class BossBehaviour : MonoBehaviour
{
    public GameObject projectile;
    public Transform projectileSpawnPos;
    public Animator anim;

    private BossHealth hp;
    private bool halfHp;
    void Awake()
    {
        hp = GetComponent<BossHealth>();
    }

    void Update()
    {
        //if boss is half health shoot another projectile
        if (!halfHp && hp.currentHealth <= hp.maxHealth / 2)
        {
            ShootProjectile();
            halfHp = true;
        }
    }

    public void ShootProjectile()
    {
        //shoot the assigned projectile and then play the animation
        GameObject fb = Instantiate(projectile, projectileSpawnPos.position, projectile.transform.rotation);
        anim.SetTrigger("Attack");
    }
}
