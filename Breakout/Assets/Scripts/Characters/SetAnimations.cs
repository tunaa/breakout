﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAnimations : MonoBehaviour
{
    private Animator anim;
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetFloat("xInput", Input.GetAxis("Horizontal"));
        anim.SetFloat("vel", GetComponentInParent<Rigidbody>().velocity.x);
    }
}
