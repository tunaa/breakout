﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour
{
    public float damageAmount = 1;
    public LayerMask damagableTargets;

    void OnCollisionEnter(Collision other)
    {
        DoDamage(other.gameObject);
    }

    //so it works if collider is a trigger as well
    void OnTriggerEnter(Collider other)
    {
        DoDamage(other.gameObject);
    }

    void DoDamage(GameObject target)
    {
        //make sure that it only targets the layermask of targets
        if ((damagableTargets.value & 1 << target.layer) == 1 << target.layer && target.GetComponent<HealthBase>())
        {
            var targetHP = target.GetComponent<HealthBase>();
            targetHP.Damage(damageAmount);
        }
    }
}
