# README

## Instructions

### Breakout

Breakout with a twist. You are party of Adventurers (only Knights class at the moment) who is trying to stop a Dragon from destroying your town!

- "A" and "D" or arrows key to move left and right/ game will start once you move. On Android press the left or right side of screen to move left or right.
- Kill the dragon by rebounding it's fireball back at it. If the fireball makes it past your party then you lose a life. If all lives are lost it's gameover! Becareful the Dragon shoots another fireball when its below half health!
- Fireballs damages the character that rebounds it (they are not fire proof). Don't let the characters in your party die, if they all die you lose.

### Pickups

Pick ups are spawned randomly (except classs based orbs) when crates are destroyed. They are automatically used when picked up.
Class based orbs add a member of that class to your party.

- Blue orb (Skill - Hammer Throw) - makes each knight class of your party throw a hammer.
- Green orb (Skill - Heal) - heals all members of your party
- Brown orb (class based orb) - Add a Knight to your party (Class based orbs are marked by their respective crate, in this case The Knight class has a shield on it's crate)

### Just some ideas for the future

The idea is that you can build a party of different classes to finish the level or progress the game.
Each class then contributes a skill to your pick up loot eg. Knights have hammer throw, Healers can have heal etc.
You can also have the possiblity of having your team in different positions.
eg. Knights will placed at the front with their high HP to soak up the damage, where as Healers or Mages with their low HP can be placed behind the tanks.

### ASSET STORE STUFF

Why have I used these assets?
I'm currently living in Victoria and we just extended our lockdown. So this gave me some time off work to make the game look more visually appealing.

- I used the Gaurd character from Mixamo, along with some animations, to make the Knight Class.
- https://assetstore.unity.com/packages/3d/props/weapons/fantasy-hammer-and-shield-190045 - Hammer and Shield
- https://assetstore.unity.com/packages/3d/environments/dungeons/low-poly-dungeons-lite-177937 - Environment
- https://assetstore.unity.com/packages/3d/characters/creatures/dragon-the-soul-eater-and-dragon-boar-77121 - Dragon
